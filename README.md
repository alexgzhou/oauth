# OAuth2 Server Example

This is a small example that uses [node-oauth2-server](https://github.com/thomseddon/node-oauth2-server/) and Mongoose.

## Installation

pre-run mongodb

You can run `node seed.js` to create a user account.  Run `npm start`, then go to <http://localhost:3000/> and sign in with `alex@example.com`, password: `test`.

cmd: 
1. `$ node seed.js`
2. `$ node server.js`
3. `$ node getToken.js`
or:
1. `$ node test/init.js`
2. `$ node test/oauth_grant_test.js`  // auth code mode, access restrict area
3. `$ node test/oauth_test.js`        // password mode, get refresh token
4. `$ node test/web_test.js`

将本工程集成到nodeServer中:
1. `$npm install oauth2-server`
2. 建立一个yiyangbao/models/oauth文件夹, 存放oauth模型(不包括user.js模型)
3. 需要将cookie替换成nodeServer中的token体系, How? 将req.session.userId替换成req.user._id即可(具体见./app.js, 搜索'req.user._id')
4. 授权页面制作
5. 其他功能完善(extendedGrant, revokeRefreshToken)

流程设计:
参见getToken.js文件