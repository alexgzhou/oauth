var app = require('./../app');
var assert = require('assert');
var request = require('supertest');
var models = require('./../models');

describe('Web sign in', function() {
  var cookies;
  before(function(done) {
    request(app)
      .post('/session')
      .send({
        email: 'alex@example.com',
        password: 'a'
      })
      .expect(302)
      .end(function(err, res) {
        cookies = res.headers['set-cookie'];
        console.log(err);
        console.log(cookies);
        done(err);
      });
  });

  // it('should allow login', function(done) {
  //   request(app)
  //     .post('/session')
  //     .send({
  //       email: 'alex@example.com',
  //       password: 'a'
  //     })
  //     .expect(302)
  //     .end(function(err, res) {
  //       cookies = res.headers['set-cookie'];
  //       console.log(err);
  //       console.log(cookies);
  //       done(err);
  //     });
  // });

  // it('should permit access to the secret area when signed in', function(done) {
  //   console.log(cookies);
  //   request(app)
  //     .get('/secret')
  //     .set('Cookie', cookies)  // 这里的cookies是不对的, 还包含了path和httponly属性, 不能用于校验, 因此返回code就不是200
  //     .expect(200, done);
  // });

  it('should permit access to the secret area when signed in', function(done) {
    // cookies = [ 'session=eyJ1c2VySWQiOiJhbGV4QGV4YW1wbGUuY29tIn0=; path=/; httponly',
    //             'session.sig=mN-rPqQDcrU1NmOOXg3xHopPcG8; path=/; httponly' ];
    request(app)
      .get('/secret')
      // .set('Cookie', cookies)  // 这里的cookies是不对的, 还包含了path和httponly属性(如上), 不能用于校验, 因此返回code就不是200
      .set('Cookie', 'session=eyJ1c2VySWQiOiJhbGV4QGV4YW1wbGUuY29tIn0=; session.sig=mN-rPqQDcrU1NmOOXg3xHopPcG8')
      .expect(200)
      .end(function(err, res) {
        done(err);
      });
  });
});
