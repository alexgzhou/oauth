var models = require('./../models');

var fixtures = {
  clients: [{
    clientId: 'papers3',
    clientSecret: '123',
    redirectUri: '/oauth/redirect'
  }],

  users: [{
    email: 'alex@example.com',
    hashed_password: '$2a$10$ejLJN8.Qlyf5xt7bsIZRQe5y.WAT7kZGydb0ghe2ludoPxxhFSz3C'
  }]
};

function insertData(model, fixture, cb) {
  var o = new model(fixture);
  o.save(cb);
}

var connection = models.mongoose.connection;

before(function(cb) {
  connection.on('open', function() {
    connection.db.dropDatabase(function() {
      insertData(models.User, fixtures.users[0], function() {
        insertData(models.OAuthClientsModel, fixtures.clients[0], cb);
      });
    });
  });
});
