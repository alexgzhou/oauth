var request = require('request'),
    jwt = require('jsonwebtoken'),
    fs = require('fs');

//client_id
var t_client_id = 'papers3';
//client_secret
var t_client_secret = '123';
//clientCredentials  以client_id：client_secret形式组合并转换成Base64-encoded, 必须用new Buffer定义字符串, 否则不会编码base64
//new Buffer("SGVsbG8gV29ybGQNCg==").toString("ascii"); 
var clientCredentials = new Buffer(t_client_id + ':' + t_client_secret).toString('base64');
//用户名
var t_username = 'alex@example.com';
//密码
var t_password = 'a';

var consoleLog = fs.createWriteStream('test.log', {flags: 'a'});

var cookies = 'session=eyJ1c2VySWQiOiJhbGV4QGV4YW1wbGUuY29tIn0=; session.sig=mN-rPqQDcrU1NmOOXg3xHopPcG8';

// console.log(clientCredentials);

// 发送Post请求获取Token(grant_type: 'password')
// 具体流程: 
// 1. 在第三方网页(http://www.dada360.com/)上点击nodeServer用户登录按钮;
// 2. 用户直接在第三方网页输入nodeServer的登录信息(用户名/密码), 点击登录并授权按钮, 进入如下流程; 
// 3. 第三方后台通过向nodeServer认证服务器url: 'http://' + clientCredentials + '@localhost:3000/oauth/token'发起request.post请求, 获取accessToken/refreshToken;
// 4. 第三方后台request.post返回错误则向网页显示错误信息, 正确则获得nodeServer认证服务器返回的accessToken/refreshToken并存储(同时返回给网页?);
// 5. 第三方后台(网页?)用headers: { Authorization: 'Bearer ' + accessToken }向nodeServer服务器url: 'http://localhost:3000/secret'发起请求, 获取nodeServer中的用户信息.
  request.post({  
    url: 'http://' + clientCredentials + '@localhost:3000/oauth/token',
    form: {
      grant_type: 'password',
      username: t_username,
      password: t_password,
      client_id: t_client_id,
      client_secret: t_client_secret
    }
  }, function(err0, res0, body0) {
    console.log(0, body0);
    //获得Token
    var accessToken = JSON.parse(body0).access_token;

    // console.log(jwt.decode(accessToken, {json: true}));

    request.get({
      url: 'http://localhost:3000/secret',
      headers: { Authorization: 'Bearer ' + accessToken }
    }, function(err0, res0, body0) {
      console.log(0, body0);
    });
  });

// 发送Post请求获取Token(grant_type: 'authorization_code'), 通过refreshToken获取accessToken(grant_type: 'refresh_token')
// 具体流程:
// 1. 在第三方网页(http://www.dada360.com/)上点击nodeServer用户登录按钮;
// 2. 带参数跳转到nodeServer授权页面(http://localhost:3000/oauth/authorise?response_type=code&client_id=papers3&client_secret=123&redirect_uri=http://www.dada360.com/Account/TBAccept), 实际情况中应该是https://host/#/oauth/authorise?query(Angular的路由中要获取query参数);
// 3. 如果用户在nodeServer授权页面下(localStorage或sessionStorage中)没有token, 则显示登录框, 如果有token, 则显示授权按钮;
// 4. 输入nodeServer的登录信息(用户名/密码), 点击登录并授权按钮(先发起一次login请求, 返回并存储token), 进入如下模拟流程; 或者直接点击授权按钮, 进入如下模拟流程;
// 5. nodeServer授权页面向nodeServer认证服务器url: 'http://localhost:3000/oauth/authorise'发起post授权请求(参数如下form), 成功? 带参数重定向(服务器端直接res.redirect('http://www.dada360.com/Account/TBAccept?code=' + authCode)) : 返回错误信息;
// 6. 重定向后第三方后台向nodeServer认证服务器url: 'http://' + clientCredentials + '@localhost:3000/oauth/token'发起request.post请求(参数如下form), 获取accessToken/refreshToken;
// 7. 第三方后台request.post返回错误则向网页显示错误信息, 正确则获得nodeServer认证服务器返回的accessToken/refreshToken并存储(同时返回给网页?);
// 8. 第三方后台(网页?)用headers: { Authorization: 'Bearer ' + accessToken }向nodeServer服务器url: 'http://localhost:3000/secret'发起请求, 获取nodeServer中的用户信息.
  request.post({  
    url: 'http://localhost:3000/oauth/authorise',
    headers: {
      'Cookie': cookies  // 这里替换成 Authorization: 'Bearer ' + token 就是采用nodeServer的token模式
    },
    form: {
      allow: 'yes',
      client_id: 'papers3',
      // client_secret: '123',  // 可选参数
      response_type: 'code',
      redirect_uri: '/oauth/redirect'  // 这里的redirect_uri要和OAuthClientsModel中存储的值一致(oauth2-server会校验), 实际情况下是http://www.dada360.com/Account/TBAccept
    }
  }, function(err1, res1, body1) {
    // console.log(err1);
    // console.log(res1.headers);
    // consoleLog.write(JSON.stringify(res1) + "\n\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n\n\n");
    // console.log(body1);
    var url = res1.headers.location;
    var authCode = url.substr(url.indexOf('code=') + 5);
    var accessToken;
    var refreshToken;
    // console.log(68, authCode);

    // setTimeout(function () {
      request.post({  
        url: 'http://' + clientCredentials + '@localhost:3000/oauth/token',
        form: {
          grant_type: 'authorization_code',
          code: authCode,
          client_id: 'papers3',
          client_secret: '123'//,
          // refresh_token: refreshToken,  // 可选参数
          // redirect_uri: '/oauth/redirect  // 可选参数
        }
      }, function(err1, res1, body1) {
        // consoleLog.write(JSON.stringify(res2) + "\n\n\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n\n\n");
        // console.log(err2, res2, body2);
        console.log(1, body1);

        //获得accessToken
        var accessToken = JSON.parse(body1).access_token;
        //访问授权信息
        request.get({
          url: 'http://localhost:3000/secret',
          headers: { Authorization: 'Bearer ' + accessToken }
        }, function(err1, res1, body1) {
          console.log(1, body1);
        });

        //获得refreshToken
        var refreshToken = JSON.parse(body1).refresh_token;
        //换取accessToken
        request.post({
          url: 'http://localhost:3000/oauth/token',
          form: {
            grant_type: 'refresh_token',
            // username: 'alex@example.com',
            // password: 'a',
            client_id: 'papers3',
            client_secret: '123',
            refresh_token: refreshToken
          }
        }, function(err1, res1, body1) {
          console.log(1.1, body1);
        });
      });
    // }, 10);
  });