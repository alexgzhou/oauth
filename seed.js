// var app = require('./app');
var models = require('./models');

models.User.create({
  email: 'alex@example.com',
  hashed_password: '$2a$10$ejLJN8.Qlyf5xt7bsIZRQe5y.WAT7kZGydb0ghe2ludoPxxhFSz3C'
}, function() {
  models.OAuthClientsModel.create({
    clientId: 'papers3',
    clientSecret: '123',
    redirectUri: '/oauth/redirect'
  }, function() {
    process.exit();
  });
});
