var express = require('express');
var routes = require('./routes');
var config = require('./config');
var path = require('path');
var models = require('./models');
var middleware = require('./middleware');
var app = express();
var oauthserver = require('node-oauth2-server');
var User = models.User;
var cookieParser = require('cookie-parser');
var cookieSession = require('cookie-session');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var logger = require('morgan');
var errorhandler = require('errorhandler');

app.set('env', process.env.NODE_ENV || 'development');
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(cookieParser('ncie0fnft6wjfmgtjz8i'));
app.use(cookieSession({
  name: 'session',
  keys: ['key1', 'key2']
}));

app.locals.title = 'OAuth Example';
app.locals.pretty = true;

// app.configure('development', 'production', function() {
  app.use(logger('dev'));
// });

app.use(bodyParser.urlencoded({ extended: true })); 
app.use(bodyParser.json({ limit: config.bodyParserJSONLimit }));
app.use(methodOverride());

app.oauth = oauthserver({  // 初始化oauth中间件
  model: models.oauth,
  grants: ['password', 'authorization_code', 'refresh_token'],
  debug: true
});

// app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(err, req, res, next) {
  if (process.env.NODE_ENV !== 'test')
    console.error('Error:', err);

  if (middleware.isValidationError(err)) {
    res.status(400);
    res.send(err.errors);
  } else {
    res.status(err.code || 500);
    res.send('Error');
  }
});

if ('development' === app.get('env')) {
  app.use(errorhandler());
}

app.get('/', middleware.loadUser, routes.index);

app.all('/oauth/token', app.oauth.grant());  // 授权中间件, 用于获取token, 包括refreshToken和accessToken

app.get('/oauth/authorise', function(req, res, next) {
  if (!req.session.userId) {
    // return res.redirect('/session?redirect=' + req.path + '&client_id=' + req.query.client_id + '&redirect_uri=' + req.query.redirect_uri);
    return res.redirect('/session?redirect=' + req.path + '&client_id=papers3' + '&redirect_uri=/oauth/redirect');
  }

  console.log(74, req.query);

  res.render('authorise', {
    client_id: req.query.client_id,
    redirect_uri: req.query.redirect_uri
  });
});

// Handle authorise
app.post('/oauth/authorise', 
  // tokenManager.verifyToken({credentialsRequired: false}),  // 这里要加上token验证, 以便获得req.user用于token模式
  function(req, res, next) {
    if (!req.session.userId) {
      // return res.redirect('/session?redirect=' + req.path + '&client_id=' + req.query.client_id + '&redirect_uri=' + req.query.redirect_uri);
      return res.redirect('/session?redirect=' + req.path + '&client_id=papers3' + '&redirect_uri=/oauth/redirect');
      console.log(88, req.query);
    }

    console.log(90, req.session, req.body);

    next();
  }, 
  app.oauth.authCodeGrant(function(req, next) {  // 授权码中间件, 用于获取authCode
    console.log(94, req.session.userId);
    // The first param should to indicate an error
    // The second param should a bool to indicate if the user did authorise the app
    // The third param should for the user/uid (only used for passing to saveAuthCode)
    next(null, req.body.allow === 'yes', req.session.userId, null);  // 这里将req.session.userId替换成req.user._id即可从cookie模式转换成token模式
  })
);

app.get('/secret', middleware.requiresUser, function(req, res) {  // middleware.requiresUser调用app.oauth.authorise()权限验证中间件, 用于验证accessToken
  res.send('Secret area');
});

app.use(app.oauth.errorHandler());

app.post('/v1/users', routes.users.create);
app.get('/account', middleware.requiresUser, routes.users.show);
app.post('/session', routes.session.create);
app.get('/session', routes.session.show);

module.exports = app;
